function Repeater(callBacks, delay, key) {
    var _$this = this;
    var _inst = undefined;
    var _d = _createValue(delay, key);
    var _stopCalbacks = {
        isset: false,
        functions: undefined
    };
    var _timer = {
        isset: false,
        delay: 0,
        current_time: 0
    };
    var repeatNumber = {
        isset: false,
        cnt: 0
    };
    this.setRepeatNumber = function (cnt) {
        repeatNumber.isset = true;
        repeatNumber.cnt = cnt;
    };
    this.setStopCallBacks = function (funcs) {
        _stopCalbacks.isset = true;
        _stopCalbacks.functions = funcs;
    };
    this.setTimer = function (t, key_timer) {
        _timer.isset = true;
        _timer.delay = _createValue(t, key_timer);
        _timer.current_time = new Date();
    };
    this.start = function (func) {
        if (callBacks == undefined) {
            throw 'Repeatable functions are not defined';
        }
        if (func != undefined) {
            _repeatCallBacks(func);
        }
        _inst = setInterval(function () {
            _repeatCallBacks(callBacks);
            if (_timer.isset) {
                if ((new Date()) - _timer.current_time >= _timer.delay) {
                    _$this.stop();
                    _timer.isset = false;
                }
            }
            if (repeatNumber.isset) {
                if (!--repeatNumber.cnt) {
                    _$this.stop();
                    repeatNumber.isset = false;
                }
            }
        }, _d);
    };
    this.setDelay = function(val, key){
        _d = _createValue(val, key);
    };
    this.stop = function (func) {
        clearInterval(_inst);
        _inst = undefined;
        _timer.isset = false;
        if (func != undefined) {
            _repeatCallBacks(func);
        }
        if (_stopCalbacks.isset) {
            _repeatCallBacks(_stopCalbacks.functions);
            _stopCalbacks.isset = false;
            _stopCalbacks.functions = undefined;
        }
    };
    this.isStoped = function () {
        return _inst == undefined;
    };

    function _repeatCallBacks(functions) {
        if (Array.isArray(functions))
            functions.forEach(function (elem) {
                elem();
            });
        else
            functions();
    }

    function _createValue(val, k) {
        switch (k) {
            case 'd':
                val *= 24;
            case 'h':
                val *= 60;
            case 'm':
                val *= 60;
            case 's':
                val *= 1000;
                break;
            default:
                val *= 1;
        }
        return val;
    }
}